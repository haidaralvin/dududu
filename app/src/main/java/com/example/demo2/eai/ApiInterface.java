package com.example.demo2.eai;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("top-headlines")
    Call<Model> getModel(@Query("country") String country,
                         @Query("category") String category,
                         @Query("apiKey") String apiKey);



}
